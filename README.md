sqlpager
========

This will add some fancy colors, emphasis as well as *boolean readability* to
the output of your command line sql client. The boolean things is because `psql`
only show a plain white lower case `f` or `t` for boolean which I think is
ridiculously unreadable, at least if they could make it uppercase. It also make
your input in a different color and output everything in a `less` with option to
not clear the screen, not wrap and close automatically if the output is not big
enough. All that without any noticable loss of speed.

**In every case, make sure you set the file as executable.**


For `psql`:
---------

Edit the file `$HOME/.psqlrc` and add the following line :

>>> `\setenv PAGER <path_to>/sqlpager` 

Get better results adding `\pset pager always` as well.


For `mysql`:
----------

Edit the file `$HOME/.my.cnf`, in the `[client]` section, add the following line:

>>> `pager = <path_to>/sqlpager`
